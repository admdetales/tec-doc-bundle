<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider;

use Nfq\Bundle\TecDocBundle\Helpers\Str;
use Symfony\Component\HttpFoundation\RequestStack;

class LocaleProvider
{
    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var string
     */
    protected $defaultLocale;

    /**
     * @param RequestStack $requestStack
     * @param string $defaultLocale
     */
    public function __construct(RequestStack $requestStack, string $defaultLocale)
    {
        $this->requestStack = $requestStack;
        $this->defaultLocale = $defaultLocale;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        $currentRequest = $this->requestStack->getCurrentRequest();

        if (null === $currentRequest) {
            return $this->defaultLocale;
        }
        
        $locale = $currentRequest->getLocale();

        if (Str::contains($locale, '_')) {
            return \explode('_', $locale)[0];
        }

        return $locale;
    }
}
