<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider;

use Nfq\Bundle\TecDocBundle\Exception\PartMountingPlaceNotFound;
use Nfq\Bundle\TecDocBundle\Model\PartMountingPlaceTable;

class MountingPlaceProvider
{
    /**
     * @var LocaleProvider
     */
    protected $localeProvider;

    /**
     * @var MountingPlaceTableProviderInterface
     */
    protected $mountingPlaceTableProvider;

    /**
     * @var PartMountingPlaceTable
     */
    protected $partMountingPlaceTable;

    /**
     * @param LocaleProvider $localeProvider
     * @param MountingPlaceTableProviderInterface $mountingPlaceTableProvider
     */
    public function __construct(
        LocaleProvider $localeProvider,
        MountingPlaceTableProviderInterface $mountingPlaceTableProvider
    ) {
        $this->localeProvider = $localeProvider;
        $this->mountingPlaceTableProvider = $mountingPlaceTableProvider;
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getMountingPlace(string $key): ?string
    {
        try {
            return $this->getPartMountingPlaceTable()->getMountingPlace($this->localeProvider->getLocale(), $key);
        } catch (PartMountingPlaceNotFound $e) {
            return null;
        }
    }

    /**
     * @return PartMountingPlaceTable
     */
    private function getPartMountingPlaceTable(): PartMountingPlaceTable
    {
        if (null === $this->partMountingPlaceTable) {
            $this->partMountingPlaceTable = $this->mountingPlaceTableProvider->getPartMountingPlaceTable();

            return $this->partMountingPlaceTable;
        }

        return $this->partMountingPlaceTable;
    }
}
