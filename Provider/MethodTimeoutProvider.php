<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider;

use Nfq\Bundle\TecDocBundle\DependencyInjection\Configuration;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;

class MethodTimeoutProvider
{
    /**
     * @var array
     */
    private $methodTimeout = [];

    /**
     * @param array $methodTimeout
     */
    public function __construct(array $methodTimeout = [])
    {
        foreach ($methodTimeout as $item) {
            $this->addMethodTimeout($item);
        }
    }

    /**
     * @param string $method
     * @return int|null
     */
    public function getTimeout(string $method): ?int
    {
        if (Arr::keyExists($method, $this->methodTimeout)) {
            return (int)$this->methodTimeout[$method];
        }

        return null;
    }

    /**
     * @param array $methodTimeout
     */
    private function addMethodTimeout(array $methodTimeout): void
    {
        $method = $methodTimeout[Configuration::METHOD_TIMEOUT_METHOD];
        $timeout = $methodTimeout[Configuration::METHOD_TIMEOUT_TIMEOUT];
        $this->methodTimeout[$method] = $timeout;
    }
}
