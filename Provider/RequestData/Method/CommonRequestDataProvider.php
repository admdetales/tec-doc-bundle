<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Provider\RequestData\Method;

use Nfq\Bundle\TecDocBundle\Helpers\Arr;
use Nfq\Bundle\TecDocBundle\Provider\LocaleProvider;

class CommonRequestDataProvider implements MethodRequestDataProviderInterface
{
    /**
     * @var LocaleProvider
     */
    protected $localeProvider;

    /**
     * @var array
     */
    private $commonRequestData;

    /**
     * @param LocaleProvider $localeProvider
     * @param array $commonRequestData
     */
    public function __construct(LocaleProvider $localeProvider, array $commonRequestData)
    {
        $this->localeProvider = $localeProvider;
        $this->commonRequestData = $commonRequestData;
    }

    /**
     * {@inheritdoc}
     */
    public function providesForMethod(string $method): bool
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function getRequestData(string $method): array
    {
        if (!Arr::keyExists('lang', $this->commonRequestData)) {
            $this->commonRequestData['lang'] = $this->localeProvider->getLocale();
        }

        return $this->commonRequestData;
    }
}
