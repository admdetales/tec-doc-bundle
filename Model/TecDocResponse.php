<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Model;

use Symfony\Component\HttpFoundation\Response;

class TecDocResponse
{
    /**
     * @var \stdClass[]
     */
    private $data;

    /**
     * @var int
     */
    private $status;

    /**
     * @param \stdClass[] $data
     * @param int $status
     */
    public function __construct(array $data, int $status)
    {
        $this->data = $data;
        $this->status = $status;
    }

    /**
     * @return \stdClass[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return Response::HTTP_OK === $this->getStatus();
    }
}
