<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Model;

use Nfq\Bundle\TecDocBundle\Exception\PartMountingPlaceNotFound;

final class PartMountingPlaceTable
{
    /**
     * @var array
     */
    protected $mountingPlaceTable;

    /**
     * PartMountingPlaceTable constructor.
     */
    public function __construct()
    {
        $this->mountingPlaceTable = [];
    }

    /**
     * @param string $locale
     * @param string $key
     * @return null|string
     * @throws PartMountingPlaceNotFound
     */
    public function getMountingPlace(string $locale, string $key)
    {
        if (!isset($this->mountingPlaceTable[$locale])) {
            $this->throwPartNotFoundException($locale, $key);
        }

        if (!isset($this->mountingPlaceTable[$locale][$key])) {
            $this->throwPartNotFoundException($locale, $key);
        }

        return $this->mountingPlaceTable[$locale][$key];
    }

    /**
     * @param string $locale
     * @param string $key
     * @param string $mountingPlace
     */
    public function setMountingPlace(string $locale, string $key, string $mountingPlace)
    {
        $this->mountingPlaceTable[$locale][$key] = $mountingPlace;
    }

    /**
     * @param string $locale
     * @param string $key
     * @throws PartMountingPlaceNotFound
     */
    protected function throwPartNotFoundException(string $locale, string $key)
    {
        throw new PartMountingPlaceNotFound(
            \sprintf('Mounting place not found by these parameters: %s, %s', $locale, $key)
        );
    }

    /**
     * @return array
     */
    public function getMountingPlaceTable(): array
    {
        return $this->mountingPlaceTable;
    }
}
