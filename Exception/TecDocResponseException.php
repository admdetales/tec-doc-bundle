<?php

/**
 * @copyright C UAB NFQ Technologies
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Exception;

use Nfq\Bundle\TecDocBundle\Model\TecDocResponse;

class TecDocResponseException extends TecDocException
{
    /**
     * @var TecDocResponse
     */
    private $response;

    /**
     * {@inheritdoc}
     */
    public function __construct(
        TecDocResponse $response,
        string $message = '',
        int $code = 0,
        \Throwable $previous = null
    ) {
        $this->response = $response;

        parent::__construct($message, $code, $previous);
    }

    /**
     * @return TecDocResponse
     */
    public function getResponse(): TecDocResponse
    {
        return $this->response;
    }
}
