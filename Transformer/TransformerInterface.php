<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Transformer;

use Nfq\Bundle\TecDocBundle\Entity\Article;

interface TransformerInterface
{
    /**
     * @param mixed $data
     * @return Article
     */
    public function transform($data): Article;

    /**
     * @param array $data
     * @return Article[]
     */
    public function transformMultiple(array $data): array;

    /**
     * @param array $data
     * @return Article[]
     */
    public function transformInitial(array $data): array;
}
