<?php

declare(strict_types=1);

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Transformer;

class IterableTransformer
{
    private const VALUES_SEPARATOR = '|';

    /**
     * @param iterable $values
     * @return string
     */
    public function transformToHash(iterable $values): string
    {
        return \md5($this->transformToString($values));
    }

    /**
     * @param iterable $values
     * @return string
     */
    private function transformToString(iterable $values): string
    {
        $flattenedValues = [];
        foreach ($values as $key => $value) {
            $flattenedValues[$key] = \preg_replace('/\s+/', '', \trim($this->valueToString($value)));
        }

        \ksort($flattenedValues);

        return \implode(self::VALUES_SEPARATOR, $flattenedValues);
    }

    /**
     * @param mixed $value
     * @return string
     */
    private function valueToString($value): string
    {
        if (\is_resource($value)) {
            throw new \InvalidArgumentException('Cannot transform resource to string');
        }

        if (\is_iterable($value)) {
            return $this->transformToString($value);
        }

        if (\is_object($value)) {
            return $this->objectToString($value);
        }

        if (\is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if (null === $value) {
            return 'null';
        }

        return (string)$value;
    }

    /**
     * @param object $object
     * @return string
     */
    private function objectToString($object): string
    {
        if (\method_exists($object, '__toString')) {
            return (string)$object;
        }

        if ($object instanceof \JsonSerializable) {
            return (string)\json_encode($object, \JSON_THROW_ON_ERROR, 512);
        }

        if ($object instanceof \Serializable || \method_exists($object, '__sleep')) {
            return \serialize($object);
        }

        return \sprintf('(object)%s', \get_class($object));
    }
}
