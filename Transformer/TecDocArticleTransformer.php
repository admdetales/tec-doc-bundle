<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Transformer;

use Nfq\Bundle\TecDocBundle\Entity\Article;

class TecDocArticleTransformer implements TransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($data): Article
    {
        return Article::createFromTecDocItem($data);
    }

    /**
     * {@inheritdoc}
     */
    public function transformInitial(array $data): array
    {
        return \array_map(
            static function (\stdClass $article): Article {
                return Article::createFromSimpleTecDocItem($article);
            },
            $data
        );
    }

    /**
     * {@inheritdoc}
     */
    public function transformMultiple(array $data): array
    {
        return \array_map(
            function (\stdClass $article): Article {
                return $this->transform($article);
            },
            $data
        );
    }
}
