<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Twig;

use Nfq\Bundle\TecDocBundle\Provider\MountingPlaceProvider;

class VehiclePartMountingPlaceExtension extends \Twig_Extension
{
    /**
     * @var MountingPlaceProvider
     */
    protected $mountingPlaceProvider;

    /**
     * @param MountingPlaceProvider $mountingPlaceProvider
     */
    public function __construct(MountingPlaceProvider $mountingPlaceProvider)
    {
        $this->mountingPlaceProvider = $mountingPlaceProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getMountingPlace', [$this, 'getMountingPlace']),
        ];
    }

    /**
     * @param string $key
     * @return null|string
     */
    public function getMountingPlace(string $key): ?string
    {
        return $this->mountingPlaceProvider->getMountingPlace($key);
    }
}
