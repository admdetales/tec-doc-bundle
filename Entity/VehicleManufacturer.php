<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class VehicleManufacturer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $title;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return VehicleManufacturer;
     */
    public function setId(int $id): VehicleManufacturer
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return VehicleManufacturer;
     */
    public function setTitle(string $title): VehicleManufacturer
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return VehicleManufacturer
     */
    public static function createFromTecDocItem(\stdClass $item): VehicleManufacturer
    {
        $manufacturer = new self();

        $manufacturer->setId($item->manuId);
        $manufacturer->setTitle($item->manuName);

        return $manufacturer;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }
}
