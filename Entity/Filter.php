<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class Filter
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var FilterValue[]
     */
    protected $values;

    /**
     * Filter constructor.
     */
    public function __construct()
    {
        $this->values = [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Filter
     */
    public function setId(int $id): Filter
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): Filter
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return FilterValue[]
     */
    public function getValues(): array
    {
        return $this->values;
    }

    /**
     * @return bool
     */
    public function hasValues(): bool
    {
        return \count($this->values) > 0;
    }

    /**
     * @param FilterValue $value
     * @return Filter
     */
    public function addValue(FilterValue $value): Filter
    {
        $this->values[] = $value;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return Filter
     */
    public static function createFromTecDocItem(\stdClass $item): Filter
    {
        $filter = new Filter();

        $filter->setId($item->attributeId);
        $filter->setName($item->attributeName);

        return $filter;
    }
}
