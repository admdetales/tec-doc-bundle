<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class OeNumber
{
    /**
     * @var string
     */
    protected $number;

    /**
     * @var string
     */
    protected $brand;

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return OeNumber
     */
    public function setNumber($number): OeNumber
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     * @return OeNumber
     */
    public function setBrand($brand): OeNumber
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return OeNumber
     */
    public static function createFromTecDocItem(\stdClass $item): OeNumber
    {
        $oeNumber = new self();
        $oeNumber->setNumber($item->oeNumber);
        $oeNumber->setBrand($item->brandName);

        return $oeNumber;
    }
}
