<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class GenericArticleByManufacturer6
{
    /**
     * @var string
     */
    private $articleNormName;

    /**
     * @var string|null
     */
    private $brandName;

    /**
     * @var string|null
     */
    private $brandNo;

    /**
     * @var int
     */
    private $genericArticleId;

    /**
     * @return string
     */
    public function getArticleNormName(): string
    {
        return $this->articleNormName;
    }

    /**
     * @param string $articleNormName
     * @return GenericArticleByManufacturer6
     */
    public function setArticleNormName(string $articleNormName): GenericArticleByManufacturer6
    {
        $this->articleNormName = $articleNormName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    /**
     * @param null|string $brandName
     * @return GenericArticleByManufacturer6
     */
    public function setBrandName(?string $brandName): GenericArticleByManufacturer6
    {
        $this->brandName = $brandName;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getBrandNo(): ?string
    {
        return $this->brandNo;
    }

    /**
     * @param null|string $brandNo
     * @return GenericArticleByManufacturer6
     */
    public function setBrandNo(?string $brandNo): GenericArticleByManufacturer6
    {
        $this->brandNo = $brandNo;

        return $this;
    }

    /**
     * @return int
     */
    public function getGenericArticleId(): int
    {
        return $this->genericArticleId;
    }

    /**
     * @param int $genericArticleId
     * @return GenericArticleByManufacturer6
     */
    public function setGenericArticleId(int $genericArticleId): GenericArticleByManufacturer6
    {
        $this->genericArticleId = $genericArticleId;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return GenericArticleByManufacturer6
     */
    public static function createFromTecDocItem(\stdClass $item): GenericArticleByManufacturer6
    {
        $object = new static();
        $object->setArticleNormName((string)$item->articleNormName);
        $object->setGenericArticleId((int)$item->genericArticleId);

        if (isset($item->brandName)) {
            $object->setBrandName((string)$item->brandName);
        }

        if (isset($item->brandNo)) {
            $object->setBrandNo($item->brandNo);
        }

        return $object;
    }
}
