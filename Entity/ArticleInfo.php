<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Helpers\Str;

class ArticleInfo
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $type;

    /**
     * @var string
     */
    protected $typeName;

    /**
     * @var string
     */
    protected $text;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ArticleInfo
     */
    public function setId(int $id): ArticleInfo
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return ArticleInfo
     */
    public function setType(int $type): ArticleInfo
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTypeName(): string
    {
        return $this->typeName;
    }

    /**
     * @param string $typeName
     * @return ArticleInfo
     */
    public function setTypeName(string $typeName): ArticleInfo
    {
        $this->typeName = $typeName;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return ArticleInfo
     */
    public function setText(string $text): ArticleInfo
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return ArticleInfo
     */
    public static function createFromTecDocItem(\stdClass $item): ArticleInfo
    {
        $articleInfo = new self();

        $articleInfo->setId($item->infoId);
        $articleInfo->setType($item->infoType);
        $articleInfo->setTypeName(Str::ucfirst($item->infoTypeName));
        $articleInfo->setText(Str::ucfirst($item->infoText));

        return $articleInfo;
    }
}
