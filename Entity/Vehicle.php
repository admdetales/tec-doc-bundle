<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class Vehicle
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var VehicleManufacturer
     */
    protected $manufacturer;

    /**
     * @var VehicleModel
     */
    protected $model;

    /**
     * @var float|null
     */
    protected $cylinderCapacity;

    /**
     * @var int
     */
    protected $powerHpFrom;

    /**
     * @var int
     */
    protected $powerHpTo;

    /**
     * @var int
     */
    protected $powerKwFrom;

    /**
     * @var int
     */
    protected $powerKwTo;

    /**
     * @var string|null
     */
    protected $constructionType;

    /**
     * @var int|null
     */
    protected $cylinders;

    /**
     * @var string|null
     */
    protected $fuelType;

    /**
     * @var string
     */
    protected $impulsionType;

    /**
     * @var int|null
     */
    protected $valves;

    /**
     * @var Attribute[]
     */
    protected $productAttributes;

    /**
     * Vehicle constructor.
     */
    public function __construct()
    {
        $this->productAttributes = [];
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Vehicle
     */
    public function setId(int $id): Vehicle
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Vehicle
     */
    public function setDescription(string $description): Vehicle
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return VehicleManufacturer
     */
    public function getManufacturer(): VehicleManufacturer
    {
        return $this->manufacturer;
    }

    /**
     * @param VehicleManufacturer $manufacturer
     * @return Vehicle
     */
    public function setManufacturer(VehicleManufacturer $manufacturer): Vehicle
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return VehicleModel
     */
    public function getModel(): VehicleModel
    {
        return $this->model;
    }

    /**
     * @param VehicleModel $model
     * @return Vehicle
     */
    public function setModel(VehicleModel $model): Vehicle
    {
        $this->model = $model;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCylinderCapacity(): ?float
    {
        return null !== $this->cylinderCapacity ? (float)\number_format($this->cylinderCapacity * 0.001, 1) : null;
    }

    /**
     * @param float|null $cylinderCapacity
     * @return Vehicle
     */
    public function setCylinderCapacity(float $cylinderCapacity = null): Vehicle
    {
        $this->cylinderCapacity = $cylinderCapacity;

        return $this;
    }

    /**
     * @return int
     */
    public function getPowerHpFrom(): int
    {
        return $this->powerHpFrom;
    }

    /**
     * @param int $powerHpFrom
     * @return Vehicle
     */
    public function setPowerHpFrom(int $powerHpFrom): Vehicle
    {
        $this->powerHpFrom = $powerHpFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getPowerHpTo(): int
    {
        return $this->powerHpTo;
    }

    /**
     * @param int $powerHpTo
     * @return Vehicle
     */
    public function setPowerHpTo(int $powerHpTo): Vehicle
    {
        $this->powerHpTo = $powerHpTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getPowerKwFrom(): int
    {
        return $this->powerKwFrom;
    }

    /**
     * @param int $powerKwFrom
     * @return Vehicle
     */
    public function setPowerKwFrom(int $powerKwFrom): Vehicle
    {
        $this->powerKwFrom = $powerKwFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getPowerKwTo(): int
    {
        return $this->powerKwTo;
    }

    /**
     * @param int $powerKwTo
     * @return Vehicle
     */
    public function setPowerKwTo(int $powerKwTo): Vehicle
    {
        $this->powerKwTo = $powerKwTo;

        return $this;
    }

    /**
     * @return Attribute[]
     */
    public function getProductAttributes(): array
    {
        return $this->productAttributes;
    }

    /**
     * @param Attribute $attribute
     * @return Vehicle
     */
    public function addProductAttribute(Attribute $attribute): Vehicle
    {
        $this->productAttributes[] = $attribute;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getConstructionType()
    {
        return $this->constructionType;
    }

    /**
     * @param string|null $constructionType
     */
    public function setConstructionType($constructionType)
    {
        $this->constructionType = $constructionType;
    }

    /**
     * @return int|null
     */
    public function getCylinders()
    {
        return $this->cylinders;
    }

    /**
     * @param int|null $cylinders
     */
    public function setCylinders($cylinders)
    {
        $this->cylinders = $cylinders;
    }

    /**
     * @return string|null
     */
    public function getFuelType()
    {
        return $this->fuelType;
    }

    /**
     * @param string|null $fuelType
     */
    public function setFuelType($fuelType)
    {
        $this->fuelType = $fuelType;
    }

    /**
     * @return int|null
     */
    public function getValves()
    {
        return $this->valves;
    }

    /**
     * @param int|null $valves
     */
    public function setValves($valves)
    {
        $this->valves = $valves;
    }

    /**
     * @param \stdClass $item
     * @return Vehicle
     */
    public static function createFromTecDocItem(\stdClass $item): Vehicle
    {
        $manufacturer = new VehicleManufacturer();
        $manufacturer->setId($item->manuId);
        $manufacturer->setTitle($item->manuDesc);

        $model = VehicleModel::createFromTecDocItem($item, VehicleModel::DATA_SOURCE_VEHICLE);

        $vehicle = new self();

        $vehicle->setId($item->carId);
        $vehicle->setDescription($item->carDesc);
        $vehicle->setManufacturer($manufacturer);
        $vehicle->setModel($model);

        $vehicle->setCylinderCapacity($item->cylinderCapacity ?? null);
        $vehicle->setPowerHpFrom($item->powerHpFrom);
        $vehicle->setPowerHpTo($item->powerHpTo);
        $vehicle->setPowerKwFrom($item->powerKwFrom);
        $vehicle->setPowerKwTo($item->powerKwTo);

        $vehicle->setConstructionType($item->constructionType ?? null);
        $vehicle->setCylinders($item->cylinder ?? null);
        $vehicle->setFuelType($item->fuelType ?? null);
        $vehicle->setValves($item->valves ?? null);

        return $vehicle;
    }
}
