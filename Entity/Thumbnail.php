<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class Thumbnail
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var int
     */
    protected $typeId;

    /**
     * @param int $id
     * @param string $fileName
     * @param int $typeId
     */
    public function __construct(int $id, string $fileName, int $typeId)
    {
        $this->id = $id;
        $this->fileName = $fileName;
        $this->typeId = $typeId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Thumbnail;
     */
    public function setId(int $id): Thumbnail
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return Thumbnail;
     */
    public function setFileName(string $fileName): Thumbnail
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     * @return Thumbnail;
     */
    public function setTypeId(int $typeId): Thumbnail
    {
        $this->typeId = $typeId;

        return $this;
    }

    /**
     * @param \stdClass $item
     * @return Thumbnail
     */
    public static function createFromTecDocItem(\stdClass $item): Thumbnail
    {
        return new self($item->thumbDocId, $item->thumbFileName, $item->thumbTypeId);
    }
}
