<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity\Collections;

use Nfq\Bundle\TecDocBundle\Helpers\Arr;

abstract class AbstractCollection implements \Countable, \IteratorAggregate, \ArrayAccess
{
    /**
     * An array containing the entries of this collection.
     *
     * @var array
     */
    protected $elements = [];

    /**
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        $this->assertElements($elements);
        $this->elements = $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return Arr::keyExists($offset, $this->elements);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->elements[$offset] : null;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if (!isset($offset)) {
            return $this->add($value);
        }

        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        return $this->remove($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function count(): int
    {
        return \count($this->elements);
    }

    /**
     * @param string|int $key
     * @param mixed $element
     */
    public function set($key, $element)
    {
        $this->assertElement($element);
        $this->elements[$key] = $element;
    }

    /**
     * @param mixed $element
     */
    public function add($element)
    {
        $this->assertElement($element);
        $this->elements[] = $element;
    }

    /**
     * @param string|int $key
     * @return mixed|null
     */
    public function remove($key)
    {
        if (!$this->offsetExists($key)) {
            return null;
        }

        $removed = $this->elements[$key];
        unset($this->elements[$key]);

        return $removed;
    }

    /**
     * @param mixed $element
     * @return bool
     */
    public function contains($element)
    {
        return \in_array($element, $this->elements, true);
    }

    /**
     * @param mixed $element
     * @return bool
     */
    public function removeElement($element)
    {
        $key = \array_search($element, $this->elements, true);

        if (false === $key) {
            return false;
        }

        $this->remove($key);

        return true;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->elements;
    }

    /**
     * @param array $elements
     * @throws \InvalidArgumentException
     */
    protected function assertElements(array $elements)
    {
        foreach ($elements as $element) {
            $this->assertElement($element);
        }
    }

    /**
     * @param mixed $element
     * @throws \InvalidArgumentException
     */
    protected function assertElement($element)
    {
        if (!$this->supports($element)) {
            throw new \InvalidArgumentException(
                \sprintf('Unsupported element type provided for %s', \get_class($this))
            );
        }
    }

    /**
     * @param mixed $element
     * @return bool
     */
    abstract protected function supports($element): bool;
}
