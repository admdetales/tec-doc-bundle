<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity\Collections;

use ArrayIterator;
use Closure;

class ArrayCollection implements \Countable, \IteratorAggregate, \ArrayAccess
{
    /**
     * @var array
     */
    private $elements;

    /**
     * @param array $elements
     */
    public function __construct(array $elements = [])
    {
        $this->elements = $elements;
    }

    /**
     * @param array $elements
     * @return static
     */
    protected function createFrom(array $elements)
    {
        return new static($elements);
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->elements;
    }

    /**
     * @return mixed
     */
    public function first()
    {
        return \reset($this->elements);
    }

    /**
     * @return mixed
     */
    public function last()
    {
        return \end($this->elements);
    }

    /**
     * @return int|string|null
     */
    public function key()
    {
        return \key($this->elements);
    }

    /**
     * @return mixed
     */
    public function next()
    {
        return \next($this->elements);
    }

    /**
     * @return mixed
     */
    public function current()
    {
        return \current($this->elements);
    }

    /**
     * @param string|int $key
     * @return mixed|null
     */
    public function remove($key)
    {
        if (!isset($this->elements[$key]) && !\array_key_exists($key, $this->elements)) {
            return null;
        }

        $removed = $this->elements[$key];
        unset($this->elements[$key]);

        return $removed;
    }

    /**
     * @param mixed $element
     * @return bool
     */
    public function removeElement($element)
    {
        $key = \array_search($element, $this->elements, true);

        if ($key === false) {
            return false;
        }

        unset($this->elements[$key]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function offsetExists($offset)
    {
        return $this->containsKey($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetSet($offset, $value)
    {
        if (!isset($offset)) {
            $this->add($value);

            return;
        }

        $this->set($offset, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function offsetUnset($offset)
    {
        $this->remove($offset);
    }

    /**
     * @param string|int $key
     * @return bool
     */
    public function containsKey($key)
    {
        return isset($this->elements[$key]) || \array_key_exists($key, $this->elements);
    }

    /**
     * @param mixed $element
     * @return bool
     */
    public function contains($element)
    {
        return \in_array($element, $this->elements, true);
    }

    /**
     * @param Closure $p
     * @return bool
     */
    public function exists(Closure $p)
    {
        foreach ($this->elements as $key => $element) {
            if ($p($key, $element)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param mixed $element
     * @return bool|int|string
     */
    public function indexOf($element)
    {
        return \array_search($element, $this->elements, true);
    }

    /**
     * @param string|int $key
     * @return mixed|null
     */
    public function get($key)
    {
        return $this->elements[$key] ?? null;
    }

    /**
     * @return array
     */
    public function getKeys()
    {
        return \array_keys($this->elements);
    }

    /**
     * @return array
     */
    public function getValues()
    {
        return \array_values($this->elements);
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return \count($this->elements);
    }

    /**
     * @param string|int $key
     * @param mixed $value
     */
    public function set($key, $value)
    {
        $this->elements[$key] = $value;
    }

    /**
     * @param mixed $element
     * @return bool
     */
    public function add($element)
    {
        $this->elements[] = $element;

        return true;
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return empty($this->elements);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }

    /**
     * @param Closure $func
     * @return ArrayCollection
     */
    public function map(Closure $func)
    {
        return $this->createFrom(\array_map($func, $this->elements));
    }

    /**
     * @param Closure $p
     * @return ArrayCollection
     */
    public function filter(Closure $p)
    {
        return $this->createFrom(\array_filter($this->elements, $p));
    }

    /**
     * @param Closure $p
     * @return bool
     */
    public function forAll(Closure $p)
    {
        foreach ($this->elements as $key => $element) {
            if (!$p($key, $element)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param Closure $p
     * @return array
     */
    public function partition(Closure $p)
    {
        $matches = [];
        $noMatches = [];
        foreach ($this->elements as $key => $element) {
            if ($p($key, $element)) {
                $matches[$key] = $element;
            } else {
                $noMatches[$key] = $element;
            }
        }

        return [$this->createFrom($matches), $this->createFrom($noMatches)];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return __CLASS__ . '@' . \spl_object_hash($this);
    }

    /**
     * @return void
     */
    public function clear()
    {
        $this->elements = [];
    }

    /**
     * @param int $offset
     * @param int|null $length
     * @return array
     */
    public function slice($offset, $length = null)
    {
        return \array_slice($this->elements, $offset, $length, true);
    }
}
