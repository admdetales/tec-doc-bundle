<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity\Collections;

use Nfq\Bundle\TecDocBundle\Entity\Category;
use Nfq\Bundle\TecDocBundle\Exception\CategoryNotFoundException;

/**
 * @method Category[] toArray()
 */
class CategoriesTree extends AbstractCollection
{
    /**
     * @param string $slug
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findOneBySlug(string $slug): Category
    {
        foreach ($this->toArray() as $category) {
            if ($slug === $category->getSlug()) {
                return $category;
            }

            try {
                return $category->getChildren()->findOneBySlug($slug);
            } catch (CategoryNotFoundException $e) {
                continue;
            }
        }

        throw new CategoryNotFoundException(\sprintf('Category with slug "%s" not found', $slug));
    }

    /**
     * @return Category
     * @throws CategoryNotFoundException
     */
    public function findRootCategory(): Category
    {
        foreach ($this->toArray() as $category) {
            if ($category->isRoot()) {
                return $category;
            }

            $parent = $category->getParent();
            while (null !== $parent) {
                if ($parent->isRoot()) {
                    return $parent;
                }

                $parent = $parent->getParent();
            }
        }

        throw new CategoryNotFoundException('Root category not found');
    }

    /**
     * @return Category[]
     */
    public function getAllFlatten(): array
    {
        $flattenedCategories = [];
        foreach ($this->toArray() as $category) {
            $flattenedCategories[] = $category->flatten();
        }

        return !empty($flattenedCategories) ? \array_merge(...$flattenedCategories) : [];
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($element): bool
    {
        return $element instanceof Category;
    }
}
