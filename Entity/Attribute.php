<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Helpers\DateParser;
use Nfq\Bundle\TecDocBundle\Helpers\Str;

class Attribute
{
    const TYPE_ALPHANUMERIC = 'A';
    const TYPE_DATE = 'D';
    const TYPE_KEY = 'K';
    const TYPE_NUMERIC = 'N';
    const TYPE_WITHOUT_VALUE = 'V';
    
    const ID_WEIGHT_KILOGRAMS = 212;
    const ID_WEIGHT_GRAMS = 683;
    const ID_PAIRED_NUMBER = 1564;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $shortName = '';

    /**
     * @var string
     */
    protected $type;

    /**
     * @var mixed
     */
    protected $value;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Attribute
     */
    public function setId(int $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return Str::ucfirst($this->name);
    }

    /**
     * @param string $name
     * @return Attribute
     */
    public function setName(string $name): Attribute
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return Str::ucfirst($this->shortName);
    }

    /**
     * @param string $shortName
     * @return Attribute
     */
    public function setShortName(string $shortName): Attribute
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Attribute
     */
    public function setType(string $type): Attribute
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return Attribute
     */
    public function setValue($value): Attribute
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return bool
     */
    public function isWeightAttribute(): bool
    {
        return $this->isWeightGramsAttribute() || $this->isWeightKilogramsAttribute();
    }

    /**
     * @return bool
     */
    public function isWeightGramsAttribute(): bool
    {
        return self::ID_WEIGHT_GRAMS === $this->getId();
    }

    /**
     * @return bool
     */
    public function isWeightKilogramsAttribute(): bool
    {
        return self::ID_WEIGHT_KILOGRAMS === $this->getId();
    }

    /**
     * @return bool
     */
    public function isPairedNumberAttribute(): bool
    {
        return self::ID_PAIRED_NUMBER === $this->getId();
    }

    /**
     * @param \stdClass $item
     * @return Attribute
     */
    public static function createFromTecDocItem(\stdClass $item): Attribute
    {
        $attribute = new self();

        $attribute->setId($item->attrId);
        $attribute->setName($item->attrName);

        if (isset($item->attrShortName)) {
            $attribute->setShortName($item->attrShortName);
        }

        $type = $item->attrType;
        $attribute->setType($type);

        if (self::TYPE_WITHOUT_VALUE != $type) {
            $attribute->setValue($item->attrValue);
        }

        if (self::TYPE_DATE == $type) {
            $attribute->setValue(DateParser::parseDate($item->attrValue));
        }

        return $attribute;
    }
}
