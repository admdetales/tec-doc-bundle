<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

use Nfq\Bundle\TecDocBundle\Entity\Collections\CategoriesTree;

class Category
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var int|null
     */
    private $parentId;

    /**
     * @var Category|null
     */
    private $parent;

    /**
     * @var bool
     */
    private $hasChildren;

    /**
     * @var CategoriesTree|Category[]
     */
    private $children;

    /**
     * @var int
     */
    private $level;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->hasChildren = false;
        $this->children = new CategoriesTree();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Category
     */
    public function setId(int $id): Category
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function setName(string $name): Category
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     * @return Category
     */
    public function setSlug(string $slug): Category
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    /**
     * @param int|null $parentId
     * @return Category
     */
    public function setParentId(int $parentId = null): Category
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category|null $parent
     * @return Category
     */
    public function setParent(Category $parent = null): Category
    {
        if ($this->parent !== $parent) {
            $this->parent = $parent;

            if (null !== $parent) {
                $parent->addChild($this);
            }
        }

        return $this;
    }

    /**
     * @return Category[]|iterable
     */
    public function getAllParents(): iterable
    {
        $parent = $this;
        while (null !== $parent) {
            yield $parent;

            $parent = $parent->getParent();
        }
    }

    /**
     * @return bool
     */
    public function hasChildren(): bool
    {
        return $this->hasChildren;
    }

    /**
     * @param bool $hasChildren
     * @return Category
     */
    public function setHasChildren(bool $hasChildren): Category
    {
        $this->hasChildren = $hasChildren;

        return $this;
    }

    /**
     * @return CategoriesTree|Category[]
     */
    public function getChildren(): CategoriesTree
    {
        return $this->children;
    }

    /**
     * @param Category $child
     * @return Category
     */
    public function addChild(Category $child): Category
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
            $child->setParent($this);
            $this->setHasChildren(true);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return Category
     */
    public function setLevel(int $level): Category
    {
        if ($this->level !== $level) {
            $this->level = $level;
            foreach ($this->getChildren() as $child) {
                $child->setLevel($this->level + 1);
            }
        }

        return $this;
    }

    /**
     * @return bool
     */
    public function isRoot(): bool
    {
        return null === $this->getParent() && null === $this->getParentId();
    }

    /**
     * Returns flattened children array.
     * @return Category[]
     */
    public function flatten(): array
    {
        $childrenCategories = [];
        foreach ($this->getChildren() as $childCategory) {
            $childrenCategories[] = $childCategory->flatten();
        }
        $childrenCategories = !empty($childrenCategories) ? \array_merge(...$childrenCategories) : [];

        return \array_merge([$this], $childrenCategories);
    }

    /**
     * @param \stdClass $item
     * @return Category
     */
    public static function createFromTecDocItem(\stdClass $item): Category
    {
        $category = new static();
        $category->setId((int)$item->assemblyGroupNodeId);
        $category->setName((string)$item->assemblyGroupName);
        $category->setHasChildren((bool)$item->hasChilds);

        if (isset($item->parentNodeId)) {
            $category->setParentId((int)$item->parentNodeId);
        }

        return $category;
    }
}
