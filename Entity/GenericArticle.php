<?php

declare(strict_types=1);

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Entity;

class GenericArticle
{
    /**
     * Designation of the assembly group.
     * @var string
     */
    private $assemblyGroup;

    /**
     * Designation of the generic article.
     * @var string
     */
    private $designation;

    /**
     * Generic article ID.
     * @var int
     */
    private $genericArticleId;

    /**
     * Master designation of the generic article.
     * @var string
     */
    private $masterDesignation;

    /**
     * Search tree node ID.
     * @var int|null
     */
    private $searchTreeNodeId;

    /**
     * Usage designation of the generic article.
     * @var string|null
     */
    private $usageDesignation;

    /**
     * @param string $assemblyGroup
     * @param string $designation
     * @param int $genericArticleId
     * @param string $masterDesignation
     * @param int|null $searchTreeNodeId
     * @param string|null $usageDesignation
     */
    public function __construct(
        string $assemblyGroup,
        string $designation,
        int $genericArticleId,
        string $masterDesignation,
        ?int $searchTreeNodeId,
        ?string $usageDesignation
    ) {
        $this->assemblyGroup = $assemblyGroup;
        $this->designation = $designation;
        $this->genericArticleId = $genericArticleId;
        $this->masterDesignation = $masterDesignation;
        $this->searchTreeNodeId = $searchTreeNodeId;
        $this->usageDesignation = $usageDesignation;
    }

    /**
     * @param \stdClass $item
     * @return GenericArticle
     */
    public static function createFromTecDocItem(\stdClass $item): GenericArticle
    {
        return new self(
            isset($item->assemblyGroup) ? (string)$item->assemblyGroup : '',
            isset($item->designation) ? (string)$item->designation : '',
            isset($item->genericArticleId) ? (int)$item->genericArticleId : 0,
            isset($item->masterDesignation) ? (string)$item->masterDesignation : '',
            isset($item->searchTreeNodeId) ? (int)$item->searchTreeNodeId : null,
            isset($item->usageDesignation) ? (string)$item->usageDesignation : null
        );
    }

    /**
     * @return string
     */
    public function getAssemblyGroup(): string
    {
        return $this->assemblyGroup;
    }

    /**
     * @return string
     */
    public function getDesignation(): string
    {
        return $this->designation;
    }

    /**
     * @return int
     */
    public function getGenericArticleId(): int
    {
        return $this->genericArticleId;
    }

    /**
     * @return string
     */
    public function getMasterDesignation(): string
    {
        return $this->masterDesignation;
    }

    /**
     * @return int|null
     */
    public function getSearchTreeNodeId(): ?int
    {
        return $this->searchTreeNodeId;
    }

    /**
     * @return string|null
     */
    public function getUsageDesignation(): ?string
    {
        return $this->usageDesignation;
    }
}
