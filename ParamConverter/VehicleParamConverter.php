<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ParamConverter;

use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\Exception\NotFoundException;
use Nfq\Bundle\TecDocBundle\ModelManager\VehicleManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class VehicleParamConverter implements ParamConverterInterface
{
    /**
     * @var VehicleManager
     */
    protected $vehicleManager;

    /**
     * @param VehicleManager $vehicleManager
     */
    public function __construct(VehicleManager $vehicleManager)
    {
        $this->vehicleManager = $vehicleManager;
    }

    /**
     * {@inheritdoc}
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $name = $configuration->getName();
        $vehicleId = $request->attributes->get($name);
        if (null === $vehicleId) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        try {
            $vehicle = $this->vehicleManager->getVehicleById($vehicleId);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException(\sprintf('%s object not found.', $configuration->getClass()));
        }

        $request->attributes->set($name, $vehicle);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(ParamConverter $configuration)
    {
        return Vehicle::class === $configuration->getClass();
    }
}
