<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Generator;

use Nfq\Bundle\TecDocBundle\Entity\Category;
use Symfony\Component\String\Slugger\SluggerInterface;

class CategorySlugGenerator implements CategorySlugGeneratorInterface
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var string
     */
    private $categorySlugSeparator;

    /**
     * @param SluggerInterface $slugger
     * @param string $categorySlugSeparator
     */
    public function __construct(SluggerInterface $slugger, string $categorySlugSeparator)
    {
        $this->slugger = $slugger;
        $this->categorySlugSeparator = $categorySlugSeparator;
    }

    /**
     * {@inheritdoc}
     */
    public function getSlug(Category $category): string
    {
        return \sprintf(
            '%s%s%s',
            $category->getId(),
            $this->categorySlugSeparator,
            $this->slugger->slug((string)$category->getName(), $this->categorySlugSeparator)
        );
    }
}
