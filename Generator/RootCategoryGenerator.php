<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Generator;

use Nfq\Bundle\TecDocBundle\Entity\Category;
use Nfq\Bundle\TecDocBundle\Provider\RootCategoryNameProviderInterface;

class RootCategoryGenerator implements RootCategoryGeneratorInterface
{
    /**
     * @var CategorySlugGeneratorInterface
     */
    private $slugGenerator;

    /**
     * @var RootCategoryNameProviderInterface
     */
    private $rootCategoryNameProvider;

    /**
     * @param CategorySlugGeneratorInterface $slugGenerator
     * @param RootCategoryNameProviderInterface $rootCategoryNameProvider
     */
    public function __construct(
        CategorySlugGeneratorInterface $slugGenerator,
        RootCategoryNameProviderInterface $rootCategoryNameProvider
    ) {
        $this->slugGenerator = $slugGenerator;
        $this->rootCategoryNameProvider = $rootCategoryNameProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(): Category
    {
        $category = new Category();
        $category->setId(-1);
        $category->setName($this->rootCategoryNameProvider->getName());
        $category->setSlug($this->slugGenerator->getSlug($category));

        return $category;
    }
}
