<?php

declare(strict_types=1);

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Transformer;

use Nfq\Bundle\TecDocBundle\Transformer\IterableTransformer;
use PHPUnit\Framework\TestCase;

class IterableTransformerTest extends TestCase
{
    /**
     * @test
     */
    public function testGenerate(): void
    {
        $values = [
            'array' => ['long' => 'test', 'array'],
            'iterator' => new \ArrayIterator(['B' => 'iterable', 'A' => 'ArrayIterator']),
            'numbers' => [12, 10.3, \M_PI],
            'strings' => 'some long text with spaces    ',
            'nullable' => null,
            'booleans' => ['ok' => true, 'not_ok' => false],
            'objects' => [
                'stringable' => new StringableStub(),
                'non_stringable' => new NonStringableStub(),
                'serializable' => new SerializableStub(),
                'serializable_interface' => new SerializableInterfaceStub(),
                'json_serializable_interface' => new JsonSerializableInterfaceStub(),
            ],
        ];

        $generator = new IterableTransformer();
        $this->assertSame('517e2ca250bcf9cf4bacddf58d4b6408', $generator->transformToHash($values));
    }

    /**
     * @expectedException \InvalidArgumentException
     * @expectedExceptionMessage Cannot transform resource to string
     */
    public function testGenerateException(): void
    {
        $generator = new IterableTransformer();
        $generator->transformToHash(['test' => \curl_init()]);
    }
}
