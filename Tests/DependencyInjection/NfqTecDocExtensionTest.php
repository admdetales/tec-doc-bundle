<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\DependencyInjection;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Nfq\Bundle\TecDocBundle\DependencyInjection\NfqTecDocExtension;

class NfqTecDocExtensionTest extends AbstractExtensionTestCase
{
    /**
     * @test
     */
    public function testMinimalConfiguration(): void
    {
        $this->load(
            [
                'article_country' => 'lt',
                'lang' => 'lt',
                'provider' => '20478',
                'api_key' => 'tec-doc-api-key',
                'api_endpoint_uri' => 'http://webservice-cs.tecdoc.net',
                'media_uri' => 'http://webservice-cs.tecdoc.net/media',
            ]
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.timeout', 3);
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.connect_timeout', 3);
        $this->assertContainerBuilderHasParameter(
            'nfq_tec_doc.method_timeout',
            [
                [
                    'method' => 'getArticleDirectSearchAllNumbersWithState',
                    'timeout' => 15,
                ],
                [
                    'method' => 'getArticleAccessoryList4',
                    'timeout' => 15,
                ],
            ]
        );
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.article_country', 'lt');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.lang', 'lt');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.provider', '20478');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.api_key', 'tec-doc-api-key');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.api_endpoint_uri', 'http://webservice-cs.tecdoc.net');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.media_uri', 'http://webservice-cs.tecdoc.net/media');

        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'nfq_tec_doc.cache.manager',
            0,
            'nfq_tec_doc.cache.storage.null'
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.category_slug_separator', '/');

        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'nfq_tec_doc.generator.root_category',
            1,
            'nfq_tec_doc.provider.default_root_category_name'
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.used_tecdoc_filters_ids', []);
    }

    /**
     * @test
     */
    public function testFullConfiguration()
    {
        $this->load(
            [
                'timeout' => 20,
                'connect_timeout' => 30,
                'method_timeout' => [
                    [
                        'method' => 'testMethod',
                        'timeout' => 20,
                    ],
                    [
                        'method' => 'getArticleDirectSearchAllNumbersWithState',
                        'timeout' => 21,
                    ],
                ],
                'article_country' => 'lt',
                'lang' => 'lt',
                'provider' => '20478',
                'api_key' => 'tec-doc-api-key2',
                'api_endpoint_uri' => 'http://webservice-cs.tecdoc.net',
                'media_uri' => 'http://webservice-cs.tecdoc.net/media',
                'cache_storage' => 'nfq_tec_doc.cache.storage.null',
                'category_slug_separator' => '...',
                'root_category_name_provider' => 'nfq_tec_doc.provider.default_root_category_name',
                'used_tecdoc_filters_ids' => [1, 15, 33, 7890],
            ]
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.timeout', 20);
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.connect_timeout', 30);
        $this->assertContainerBuilderHasParameter(
            'nfq_tec_doc.method_timeout',
            [
                [
                    'method' => 'getArticleDirectSearchAllNumbersWithState',
                    'timeout' => 21,
                ],
                [
                    'method' => 'testMethod',
                    'timeout' => 20,
                ],
                [
                    'method' => 'getArticleAccessoryList4',
                    'timeout' => 15,
                ],
            ]
        );
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.article_country', 'lt');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.lang', 'lt');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.provider', '20478');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.api_key', 'tec-doc-api-key2');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.api_endpoint_uri', 'http://webservice-cs.tecdoc.net');
        $this->assertContainerBuilderHasParameter('nfq_tec_doc.media_uri', 'http://webservice-cs.tecdoc.net/media');

        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'nfq_tec_doc.cache.manager',
            0,
            'nfq_tec_doc.cache.storage.null'
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.category_slug_separator', '...');

        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'nfq_tec_doc.generator.root_category',
            1,
            'nfq_tec_doc.provider.default_root_category_name'
        );

        $this->assertContainerBuilderHasParameter('nfq_tec_doc.used_tecdoc_filters_ids', [1, 15, 33, 7890]);
    }

    /**
     * {@inheritdoc}
     */
    protected function getContainerExtensions()
    {
        return [
            new NfqTecDocExtension(),
        ];
    }
}
