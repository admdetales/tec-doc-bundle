<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Provider;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Loader\YamlMountingPlaceFileLoader;
use Nfq\Bundle\TecDocBundle\Provider\YamlMountingPlaceTableProvider;

class YamlMountingPlaceTableProviderTest extends TestCase
{
    /**
     * @var YamlMountingPlaceTableProvider
     */
    protected $yamlMountingPlaceTableProvider;

    /**
     * @var YamlMountingPlaceFileLoader|MockObject
     */
    protected $fileLoaderMock;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->fileLoaderMock = $this->createMock(YamlMountingPlaceFileLoader::class);
        $this->yamlMountingPlaceTableProvider = new YamlMountingPlaceTableProvider($this->fileLoaderMock);
    }

    /**
     * @param array $dataFromYaml
     * @param array $expectedMountingPlaceTable
     * @dataProvider mountingPlaceDataProvider
     */
    public function testGetPartMountingPlaceTable(array $dataFromYaml, array $expectedMountingPlaceTable)
    {
        $this->fileLoaderMock->method('getFileData')->willReturn($dataFromYaml);

        $mountPlaceTable = $this->yamlMountingPlaceTableProvider->getPartMountingPlaceTable()->getMountingPlaceTable();

        $this->assertEquals($expectedMountingPlaceTable, $mountPlaceTable);
    }

    /**
     * @return array
     */
    public function mountingPlaceDataProvider()
    {
        return [
            [
                [
                    'mounting_places' => [
                        'en_US' => [
                            'Axle Position 1 left' => 'PK',
                            'Axle Position 1 right' => 'PD',
                            'Axle Position 2 left' => 'PK',
                            'Axle Position 2 right' => 'PD',
                            'Axle Position 5 left' => 'PK',
                            'Driver side' => 'PK',
                            'Front' => 'P',
                        ],
                        'lt_LT' => [
                            'Axle Position 1 kaire' => 'PK',
                            'Axle Position 1 desine' => 'PD',
                            'Axle Position 2 kaire' => 'PK',
                            'Axle Position 2 desine' => 'PD',
                            'Axle Position 5 kaire' => 'PK',
                            'Vairuotojo puse' => 'PK',
                            'Priekis' => 'P',
                        ],
                    ],
                ],
                [
                    'en_US' => [
                        'Axle Position 1 left' => 'PK',
                        'Axle Position 1 right' => 'PD',
                        'Axle Position 2 left' => 'PK',
                        'Axle Position 2 right' => 'PD',
                        'Axle Position 5 left' => 'PK',
                        'Driver side' => 'PK',
                        'Front' => 'P',
                    ],
                    'lt_LT' => [
                        'Axle Position 1 kaire' => 'PK',
                        'Axle Position 1 desine' => 'PD',
                        'Axle Position 2 kaire' => 'PK',
                        'Axle Position 2 desine' => 'PD',
                        'Axle Position 5 kaire' => 'PK',
                        'Vairuotojo puse' => 'PK',
                        'Priekis' => 'P',
                    ],
                ],
            ],
        ];
    }
}
