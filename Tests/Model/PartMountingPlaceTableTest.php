<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Model;

use Nfq\Bundle\TecDocBundle\Exception\PartMountingPlaceNotFound;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Model\PartMountingPlaceTable;

class PartMountingPlaceTableTest extends TestCase
{
    /**
     * @var PartMountingPlaceTable
     */
    protected $partMountingPlaceTable;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->partMountingPlaceTable = new PartMountingPlaceTable();
    }

    /**
     * @test
     * @throws PartMountingPlaceNotFound
     */
    public function testGetMountingPlace()
    {
        $this->partMountingPlaceTable->setMountingPlace('lt', 'key', 'foo');
        $this->assertEquals('foo', $this->partMountingPlaceTable->getMountingPlace('lt', 'key'));
    }

    /**
     * @expectedException \Nfq\Bundle\TecDocBundle\Exception\PartMountingPlaceNotFound
     * @throws PartMountingPlaceNotFound
     */
    public function testGetNonExistingMountingPlace()
    {
        $this->assertEquals(null, $this->partMountingPlaceTable->getMountingPlace('lt', 'key'));
    }
}
