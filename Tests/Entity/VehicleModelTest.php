<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\VehicleModel;

class VehicleModelTest extends TestCase
{
    /**
     * @test
     */
    public function testCreateFromTecDocItemFromModelSource()
    {
        $tecDocModelItem = new \stdClass();
        $tecDocModelItem->modelId = 164;
        $tecDocModelItem->modelname = '02 (E10) M';
        $tecDocModelItem->yearOfConstrFrom = 196604;
        $tecDocModelItem->yearOfConstrTo = 197707;

        $vehicleModel = VehicleModel::createFromTecDocItem($tecDocModelItem);

        $this->assertTrue($vehicleModel instanceof VehicleModel);
        $this->assertNotNull($vehicleModel->getYearOfConstructionFrom());
        $this->assertNotNull($vehicleModel->getYearOfConstructionTo());

        $this->assertEquals(164, $vehicleModel->getId());
        $this->assertEquals('02 (E10) M', $vehicleModel->getTitle());
        $this->assertEquals('1966-04', $vehicleModel->getYearOfConstructionFrom()->format('Y-m'));
        $this->assertEquals('1977-07', $vehicleModel->getYearOfConstructionTo()->format('Y-m'));
    }

    /**
     * @test
     */
    public function testCreateFromTecDocItemFromVehicleSource()
    {
        $tecDocModelItem = new \stdClass();
        $tecDocModelItem->modelId = 164;
        $tecDocModelItem->modelDesc = '02 (E10) V';
        $tecDocModelItem->yearOfConstructionFrom = 196604;
        $tecDocModelItem->yearOfConstructionTo = 197707;

        $vehicleModel = VehicleModel::createFromTecDocItem($tecDocModelItem, VehicleModel::DATA_SOURCE_VEHICLE);

        $this->assertTrue($vehicleModel instanceof VehicleModel);
        $this->assertNotNull($vehicleModel->getYearOfConstructionFrom());
        $this->assertNotNull($vehicleModel->getYearOfConstructionTo());

        $this->assertEquals(164, $vehicleModel->getId());
        $this->assertEquals('02 (E10) V', $vehicleModel->getTitle());
        $this->assertEquals('1966-04', $vehicleModel->getYearOfConstructionFrom()->format('Y-m'));
        $this->assertEquals('1977-07', $vehicleModel->getYearOfConstructionTo()->format('Y-m'));
    }

    /**
     * @expectedException \LogicException
     */
    public function testInvalidSourceType()
    {
        $tecDocModelItem = new \stdClass();
        $tecDocModelItem->modelId = 164;

        VehicleModel::createFromTecDocItem($tecDocModelItem, '123');
    }
}
