<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;
use Nfq\Bundle\TecDocBundle\Entity\Vehicle;

class VehicleTest extends TestCase
{
    /**
     * @param array $vehicleData
     * @param bool $hasYearOfConstructionFrom
     * @param bool $hasYearOfConstructionTo
     *
     * @dataProvider tecDocVehicleItemProvider
     */
    public function testCreateFromTecDocItem(
        array $vehicleData,
        bool $hasYearOfConstructionFrom,
        bool $hasYearOfConstructionTo
    ) {
        $vehicle = Vehicle::createFromTecDocItem(
            \json_decode(\json_encode($vehicleData, \JSON_THROW_ON_ERROR, 512), false, 512, \JSON_THROW_ON_ERROR)
        );

        $this->assertEquals(18811, $vehicle->getId());
        $this->assertEquals('35 visų ratų pavara', $vehicle->getDescription());
        $this->assertEquals('INFINITI', $vehicle->getManufacturer()->getTitle());
        $this->assertEquals(1526, $vehicle->getManufacturer()->getId());
        $this->assertEquals('FX', $vehicle->getModel()->getTitle());
        $this->assertEquals(5441, $vehicle->getModel()->getId());
        $this->assertEquals(3.5, $vehicle->getCylinderCapacity());

        $this->assertEquals(280, $vehicle->getPowerHpFrom());
        $this->assertEquals(280, $vehicle->getPowerHpTo());
        $this->assertEquals(206, $vehicle->getPowerKwFrom());
        $this->assertEquals(206, $vehicle->getPowerKwTo());

        $this->assertEquals(4, $vehicle->getCylinders());
        $this->assertEquals('benzinas', $vehicle->getFuelType());
        $this->assertEquals(4, $vehicle->getValves());

        if ($hasYearOfConstructionFrom) {
            $this->assertNotNull($vehicle->getModel()->getYearOfConstructionFrom());
            $this->assertNotNull($vehicle->getModel()->getYearOfConstructionTo());

            $this->assertEquals('2003-01', $vehicle->getModel()->getYearOfConstructionFrom()->format('Y-m'));
            if ($hasYearOfConstructionTo) {
                $this->assertEquals('2008-12', $vehicle->getModel()->getYearOfConstructionTo()->format('Y-m'));
            } else {
                $this->assertEquals(date('Y-m'), $vehicle->getModel()->getYearOfConstructionTo()->format('Y-m'));
            }
        } else {
            $this->assertEquals(null, $vehicle->getModel()->getYearOfConstructionFrom());
            $this->assertEquals(null, $vehicle->getModel()->getYearOfConstructionTo());
        }
    }

    /**
     * @return array
     */
    public function tecDocVehicleItemProvider()
    {
        $fromDate = 200301;
        $toDate = 200812;

        $tecDocItem = $this->getTecDocArticleItem();
        $tecDocItemWithFromDate = $this->getTecDocArticleItem();
        $tecDocItemWithToDate = $this->getTecDocArticleItem();
        $tecDocItemWithBothDates = $this->getTecDocArticleItem();

        $tecDocItemWithFromDate['yearOfConstructionFrom'] = $fromDate;
        $tecDocItemWithToDate['yearOfConstructionTo'] = $toDate;
        $tecDocItemWithBothDates['yearOfConstructionFrom'] = $fromDate;
        $tecDocItemWithBothDates['yearOfConstructionTo'] = $toDate;

        return [
            [
                $tecDocItem,
                false,
                false,
            ],
            [
                $tecDocItemWithFromDate,
                true,
                false,
            ],
            [
                $tecDocItemWithToDate,
                false,
                true,
            ],
            [
                $tecDocItemWithBothDates,
                true,
                true,
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getTecDocArticleItem()
    {
        return [
            'carDesc' => '35 visų ratų pavara',
            'carId' => 18811,
            'constructionType' => 'uždaryta visureigė transporto priemonė',
            'cylinderCapacity' => 3498,
            'linkingTargetType' => 'P',
            'manuDesc' => 'INFINITI',
            'manuId' => 1526,
            'modelDesc' => 'FX',
            'modelId' => 5441,
            'powerHpFrom' => 280,
            'powerHpTo' => 280,
            'powerKwFrom' => 206,
            'powerKwTo' => 206,
            'brakeSystem' => 'hidraulinis',
            'ccmTech' => 2922,
            'cylinder' => 4,
            'cylinderCapacityLiter' => 2.9,
            'fuelType' => 'benzinas',
            'fuelTypeProcess' => 'įsiurbimo kolektoriaus įpurškimas / karbiuratorius',
            'motorType' => 'benzininis',
            'valves' => 4,
        ];
    }

    /**
     * @test
     */
    public function testProductAttributes()
    {
        $vehicle = new Vehicle();

        $attribute = new Attribute();

        $this->assertEquals(0, count($vehicle->getProductAttributes()));
        $vehicle->addProductAttribute($attribute);
        $attributes = $vehicle->getProductAttributes();
        $this->assertEquals(1, count($attributes));

        $this->assertEquals($attribute, $attributes[0]);
    }
}
