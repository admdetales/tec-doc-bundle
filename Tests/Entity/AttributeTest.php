<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\Entity;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Attribute;

class AttributeTest extends TestCase
{
    /**
     * @param array $attributeData
     *
     * @dataProvider attributeDataProvider
     */
    public function testAttributeCreation(array $attributeData)
    {
        $attr = \json_decode(\json_encode($attributeData, JSON_THROW_ON_ERROR, 512), false, 512, JSON_THROW_ON_ERROR);

        $attribute = Attribute::createFromTecDocItem($attr);

        $this->assertEquals($attributeData['attrId'], $attribute->getId());
        $this->assertEquals(ucfirst($attributeData['attrName']), $attribute->getName());
        $this->assertEquals(ucfirst($attributeData['attrShortName']), $attribute->getShortName());
        $this->assertEquals($attributeData['attrType'], $attribute->getType());

        if ($attributeData['attrType'] == Attribute::TYPE_WITHOUT_VALUE) {
            $this->assertEquals(null, $attribute->getValue());
        } elseif ($attributeData['attrType'] == Attribute::TYPE_DATE) {
            $this->assertEquals(date('Y-m'), $attribute->getValue());
        } else {
            $this->assertEquals($attributeData['attrValue'], $attribute->getValue());
        }
    }

    /**
     * @return array
     */
    public function attributeDataProvider()
    {
        return [
            [
                [
                    'attrBlockNo' => 0,
                    'attrId' => 100,
                    'attrIsConditional' => false,
                    'attrIsInterval' => false,
                    'attrName' => 'montavimo vieta',
                    'attrShortName' => 'montavimo vieta',
                    'attrType' => 'K',
                    'attrValue' => 'priekinė ašis',
                ],
            ],
            [
                [
                    'attrBlockNo' => 0,
                    'attrId' => 100,
                    'attrIsConditional' => false,
                    'attrIsInterval' => false,
                    'attrName' => 'montavimo vieta',
                    'attrShortName' => 'montavimo vieta',
                    'attrType' => 'V',
                ],
            ],
            [
                [
                    'attrBlockNo' => 0,
                    'attrId' => 21,
                    'attrIsConditional' => false,
                    'attrIsInterval' => true,
                    'attrName' => 'Gamybos metai iki',
                    'attrShortName' => 'gamybos metai iki',
                    'attrType' => 'D',
                    'attrValue' => date('Ym'),
                ],
            ],
        ];
    }
}
