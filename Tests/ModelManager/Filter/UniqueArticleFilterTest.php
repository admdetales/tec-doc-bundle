<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\ModelManager\Filter;

use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\ModelManager\Filter\UniqueArticleFilter;

class UniqueArticleFilterTest extends TestCase
{
    /**
     * @var UniqueArticleFilter
     */
    protected $filter;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->filter = new UniqueArticleFilter();
    }

    /**
     * @dataProvider articleDataProvider
     * @param array $articles
     * @param array $expectedArticles
     */
    public function testRemoveNonUniqueArticles(array $articles, array $expectedArticles)
    {
        $output = $this->filter->filter($articles);

        $this->assertEquals($expectedArticles, $output);
    }

    /**
     * @return array
     */
    public function articleDataProvider()
    {
        $article1 = new \stdClass();
        $article1->directArticle = $this->createDirectArticle(1);
        $article2 = new \stdClass();
        $article2->directArticle = $this->createDirectArticle(2);
        $article3 = new \stdClass();
        $article3->directArticle = $this->createDirectArticle(3);
        $article4 = new \stdClass();
        $article4->directArticle = $this->createDirectArticle(4);
        $article5 = new \stdClass();

        return [
            [
                [
                    $article1,
                    $article1,
                    $article3,
                    $article4,
                ],
                [
                    $article1,
                    $article3,
                    $article4,
                ],
            ],
            [
                [
                    $article1,
                    $article1,
                    $article3,
                    $article4,
                    $article5,
                ],
                [
                    $article1,
                    $article3,
                    $article4,
                ],
            ],
            [
                [
                    $article1,
                ],
                [
                    $article1,
                ],
            ],
            [
                [
                    $article5,
                ],
                [],
            ],
            [
                [],
                [],
            ],
        ];
    }

    /**
     * @param int $id
     * @return \stdClass
     */
    protected function createDirectArticle(int $id)
    {
        $article = new \stdClass();
        $article->articleId = $id;

        return $article;
    }
}
