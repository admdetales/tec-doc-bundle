<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Helpers\Arr;
use Nfq\Bundle\TecDocBundle\ModelManager\FilterManager;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\Entity\Filter;
use Nfq\Bundle\TecDocBundle\Entity\FilterValue;

class FilterManagerTest extends TestCase
{
    /**
     * @var TecDocApiManagerInterface|MockObject
     */
    protected $apiManager;

    /**
     * @var FilterManager
     */
    protected $filterManager;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->apiManager = $this->createMock(TecDocApiManagerInterface::class);
        $this->filterManager = new FilterManager($this->apiManager, [274, 100]);
    }

    /**
     * @test
     */
    public function testGetFiltersWithArticleLinks()
    {
        $this->apiManager->method('getCriteriaDialogAttributs')->willReturn($this->getTecDocFiltersItem());

        $filtersWithArticleLinks = $this->filterManager->getFiltersWithArticleLinks(1, 2, 'P');
        
        $this->assertCount(2, $filtersWithArticleLinks);
        $this->assertTrue(Arr::keyExists(FilterManager::KEY_FILTERS, $filtersWithArticleLinks));
        $this->assertTrue(Arr::keyExists(FilterManager::KEY_ARTICLE_LINKS, $filtersWithArticleLinks));


        $filters = $filtersWithArticleLinks[FilterManager::KEY_FILTERS];

        $this->assertCount(2, $filters);

        foreach ($filters as $key => $filter) {
            $this->assertInstanceOf(Filter::class, $filter);

            $expectedValuesCount = $key == 0 ? 2 : 1;

            $this->assertCount($expectedValuesCount, $filter->getValues());

            foreach ($filter->getValues() as $value) {
                $this->assertInstanceOf(FilterValue::class, $value);
            }
        }

        $expectedArticleLinks = [
            [
                'articleId' => 1309740,
                'articleLinkId' => 171366254,
            ],
            [
                'articleId' => 1342606,
                'articleLinkId' => 171366271,
            ],
        ];

        $articleLinks = $filtersWithArticleLinks[FilterManager::KEY_ARTICLE_LINKS];

        $this->assertEquals($expectedArticleLinks, $articleLinks);
    }

    /**
     * @return mixed
     */
    protected function getTecDocFiltersItem()
    {
        $data = [
            [
                'articleLinkIds' => [
                    'array' => [
                        [
                            'articleId' => 1309740,
                            'articleLinkId' => 171366254,
                        ],
                        [
                            'articleId' => 1342606,
                            'articleLinkId' => 171366271,
                        ],
                    ],
                ],
                'attributeValues' => [
                    'array' => [
                        [
                            'attributeId' => 274,
                            'count' => 1,
                            'noValue' => false,
                            'numValue' => '19.9',
                            'selected' => false,
                            'type' => 'N',
                        ],
                        [
                            'attributeId' => 274,
                            'count' => 72,
                            'noValue' => false,
                            'numValue' => '30.0',
                            'selected' => false,
                            'type' => 'N',
                        ],
                        [
                            'attributeId' => 100,
                            'count' => 10,
                            'noValue' => false,
                            'alphaValue' => 'Galinė ašis',
                            'selected' => false,
                            'type' => 'A',
                        ],
                    ],
                ],
                'attributes' => [
                    'array' => [
                        [
                            'attributeId' => 274,
                            'attributeName' => 'važiuoklės numeris',
                            'attributeShortName' => 'Važiuokl. Nr.',
                            'attributeType' => 'A',
                            'dutyFlag' => false,
                            'interval' => false,
                        ],
                        [
                            'attributeId' => 100,
                            'attributeName' => 'montavimo vieta',
                            'attributeShortName' => 'montavimo vieta',
                            'attributeType' => 'K',
                            'dutyFlag' => true,
                            'interval' => false,
                        ],
                        [
                            'attributeId' => 1337,
                            'attributeName' => 'Ilgis',
                            'attributeShortName' => 'Ilgis',
                            'attributeType' => 'N',
                            'dutyFlag' => true,
                            'interval' => false,
                        ],
                    ],
                ],
            ],
        ];

        return \json_decode(\json_encode($data, \JSON_THROW_ON_ERROR, 512), false, 512, \JSON_THROW_ON_ERROR);
    }
}
