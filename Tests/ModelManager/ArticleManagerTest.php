<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Tests\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Entity\GenericArticle;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Nfq\Bundle\TecDocBundle\ModelManager\Filter\ArticleFilterInterface;
use Nfq\Bundle\TecDocBundle\ModelManager\ArticleManager;
use Nfq\Bundle\TecDocBundle\Transformer\TransformerInterface;

class ArticleManagerTest extends TestCase
{
    /**
     * @var TecDocApiManagerInterface|MockObject
     */
    protected $apiManager;

    /**
     * @var ArticleManager
     */
    protected $manager;

    /**
     * @var TransformerInterface|MockObject
     */
    protected $transformer;

    /**
     * @var ArticleFilterInterface|MockObject
     */
    protected $articleFilter;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->apiManager = $this->createMock(TecDocApiManagerInterface::class);
        $this->transformer = $this->createMock(TransformerInterface::class);
        $this->articleFilter = $this->createMock(ArticleFilterInterface::class);
        $this->articleFilter->method('filter')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );
        $this->manager = new ArticleManager($this->apiManager, $this->transformer, $this->articleFilter);
    }

    /**
     * @test
     */
    public function findProductsByCode()
    {
        $responseData = [
            [
                'articleId' => 1,
            ],
            [
                'articleId' => 2,
            ],
        ];

        $responseData = \json_decode(
            \json_encode($responseData, \JSON_THROW_ON_ERROR, 512),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $this->transformer->method('transformInitial')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );

        $expectedOutput = [
            (object)['articleId' => 1],
            (object)['articleId' => 2],
        ];

        $this
            ->apiManager
            ->expects($this->once())
            ->method('getArticleDirectSearchAllNumbersWithState')
            ->willReturn($responseData);

        $this->assertEquals($expectedOutput, $this->manager->findArticlesByCode('1111'));
    }

    /**
     * @param mixed $articleIds
     * @param mixed $endpointCallCount
     *
     * @dataProvider articleIdProvider
     */
    public function testFindProductsByArticleId($articleIds, $endpointCallCount)
    {
        $expectedOutput = \array_fill(0, $endpointCallCount, 10);

        $this->transformer->method('transformMultiple')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );

        $this
            ->apiManager
            ->expects($this->exactly($endpointCallCount))
            ->method('getDirectArticlesByIds7')
            ->willReturn([10]);

        $this->assertEquals($expectedOutput, $this->manager->findProductsByArticleIds($articleIds));
    }

    /**
     * @test
     */
    public function testFindProductsByOeCode()
    {
        $responseData = [
            [
                'articleId' => 1,
            ],
            [
                'articleId' => 2,
            ],
            [
                'articleId' => 3,
            ],
        ];

        $responseData = \json_decode(
            \json_encode($responseData, \JSON_THROW_ON_ERROR, 512),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $expectedOutput = [10];

        $this->transformer->method('transformMultiple')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );

        $this
            ->apiManager
            ->expects($this->once())
            ->method('getArticleDirectSearchAllNumbersWithState')
            ->willReturn($responseData);

        $this
            ->apiManager
            ->expects($this->once())
            ->method('getDirectArticlesByIds7')
            ->with([1, 2, 3])
            ->willReturn($expectedOutput);

        $this->assertEquals($expectedOutput, $this->manager->findProductsByOeCode('1234'));
    }

    /**
     * @test
     */
    public function testFindEquipmentByArticleId()
    {
        $valueMap = [
            [
                1,
                \json_decode(
                    \json_encode(
                        [
                            [
                                'partlistInfo' => [
                                    'array' => [
                                        [
                                            'partlistDetails' => [
                                                'partArticleId' => 123,
                                            ],
                                        ],
                                        [
                                            'partlistDetails' => [
                                                'partArticleId' => 456,
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        \JSON_THROW_ON_ERROR,
                        512
                    ),
                    false,
                    512,
                    \JSON_THROW_ON_ERROR
                ),
            ],
            [
                2,
                [],
            ],
        ];

        $this
            ->apiManager
            ->expects($this->exactly(2))
            ->method('getArticlePartList')
            ->will(
                $this->returnValueMap($valueMap)
            );

        $articles = \json_decode(
            \json_encode(
                [
                    [
                        'directArticle' => [
                            'articleId' => 123,
                        ],
                    ],
                    [
                        'directArticle' => [
                            'articleId' => 456,
                        ],
                    ],
                ],
                \JSON_THROW_ON_ERROR,
                512
            ),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $this
            ->apiManager
            ->expects($this->once())
            ->method('getDirectArticlesByIds7')
            ->willReturn($articles);

        $this->transformer->method('transformMultiple')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );

        $this->assertEquals($articles, $this->manager->findEquipmentByArticleId(1));
        $this->assertEquals([], $this->manager->findEquipmentByArticleId(2));
    }

    /**
     * @test
     */
    public function testFindAccessoriesByArticleId()
    {
        $valueMap = [
            [
                1,
                \json_decode(
                    \json_encode(
                        [
                            [
                                'accessoryDetails' => [
                                    'accessoryArticleId' => 123,
                                ],
                            ],
                            [
                                'accessoryDetails' => [
                                    'accessoryArticleId' => 456,
                                ],
                            ],
                        ],
                        \JSON_THROW_ON_ERROR,
                        512
                    ),
                    false,
                    512,
                    \JSON_THROW_ON_ERROR
                ),
            ],
            [
                2,
                [],
            ],
        ];

        $this
            ->apiManager
            ->expects($this->exactly(2))
            ->method('getArticleAccessoryList4')
            ->will(
                $this->returnValueMap($valueMap)
            );

        $articles = \json_decode(
            \json_encode(
                [
                    [
                        'directArticle' => [
                            'articleId' => 123,
                        ],
                    ],
                    [
                        'directArticle' => [
                            'articleId' => 456,
                        ],
                    ],
                ],
                \JSON_THROW_ON_ERROR,
                512
            ),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $this
            ->apiManager
            ->expects($this->once())
            ->method('getDirectArticlesByIds7')
            ->willReturn($articles);

        $this->transformer->method('transformMultiple')->will(
            $this->returnCallback(
                function ($value) {
                    return $value;
                }
            )
        );

        $this->assertEquals($articles, $this->manager->findAccessoriesByArticleId(1));
        $this->assertEquals([], $this->manager->findAccessoriesByArticleId(2));
    }

    /**
     * @return array
     */
    public function articleIdProvider()
    {
        return [
            [
                \range(1, 5),
                1,
            ],
            [
                \range(1, 30),
                2,
            ],
            [
                \range(1, 50),
                2,
            ],
        ];
    }

    /**
     * @dataProvider searchByCodeProvider
     * @param mixed $code
     */
    public function testSearchWithInvalidArticleCode($code)
    {
        $this->assertEquals([], $this->manager->findArticlesByCode($code));
    }

    /**
     * @dataProvider searchByCodeProvider
     * @param mixed $code
     */
    public function testSearchWithInvalidArticleOECode($code)
    {
        $this->assertEquals([], $this->manager->findProductsByOeCode($code));
    }

    /**
     * @return array
     */
    public function searchByCodeProvider()
    {
        return [
            [1],
            [12],
            [134],
            ['12%'],
        ];
    }

    /**
     */
    public function testGetGenericArticles(): void
    {
        $genericArticles = \json_decode(
            \json_encode(
                [
                    [
                        'assemblyGroup' => 'starterio sistema',
                        'designation' => 'starterio akumuliatorius',
                        'genericArticleId' => 1,
                        'masterDesignation' => 'akumuliatorius',
                    ],
                    [
                        'assemblyGroup' => 'pagrindinė pavara',
                        'designation' => 'jungčių komplektas, kardaninis velenas',
                        'genericArticleId' => 5,
                        'masterDesignation' => 'sujungimo komplektas',
                        'usageDesignation' => 'kardaninis velenas',
                    ],
                    [
                        'assemblyGroup' => '',
                    ],
                    [
                        'assemblyGroup' => 'tepimas',
                        'designation' => 'alyvos filtras',
                        'genericArticleId' => 7,
                        'masterDesignation' => 'filtras',
                        'searchTreeNodeId' => 10,
                        'usageDesignation' => 'variklio alyva',
                    ],
                ],
                \JSON_THROW_ON_ERROR,
                512
            ),
            false,
            512,
            \JSON_THROW_ON_ERROR
        );

        $this->apiManager->expects($this->once())->method('getGenericArticles')
            ->with(true, false, null, null)->willReturn($genericArticles);

        $this->assertEquals(
            [
                new GenericArticle('starterio sistema', 'starterio akumuliatorius', 1, 'akumuliatorius', null, null),
                new GenericArticle(
                    'pagrindinė pavara',
                    'jungčių komplektas, kardaninis velenas',
                    5,
                    'sujungimo komplektas',
                    null,
                    'kardaninis velenas'
                ),
                new GenericArticle('', '', 0, '', null, null),
                new GenericArticle('tepimas', 'alyvos filtras', 7, 'filtras', 10, 'variklio alyva'),
            ],
            $this->manager->getGenericArticles()
        );
    }
}
