<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle;

final class TargetType
{
    public const PASSENGER_CAR = 'P';
    public const COMMERCIAL_CAR = 'O';
    public const MOTOR = 'M';
    public const AXLES = 'A';
    public const BODY_TYPE = 'K';
    public const UNIVERSAL = 'U';

    /**
     * Forbid class initialization.
     */
    private function __construct()
    {
    }
}
