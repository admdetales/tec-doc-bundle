NfqTecDocBundle
===============
[![pipeline status](http://gitlab.nfq.lt/eoltas/tecdoc-bundle/badges/master/pipeline.svg)](http://gitlab.nfq.lt/eoltas/tecdoc-bundle/commits/master)

## Installation
```console
$ composer require nfq/tecdoc-bundle
```

## Enable the Bundle
```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Nfq\Bundle\TecDocBundle\NfqTecDocBundle(),
        );

        // ...
    }

    // ...
}
```

## Configure bundle
```yaml
nfq_tec_doc:
    timeout: 3
    connect_timeout: 3
    method_timeout:
        - { method: getArticleDirectSearchAllNumbersWithState, timeout: 15 }
        - { method: getArticleAccessoryList4, timeout: 15 }
    article_country: "lt"
    lang: "lt"
    provider: "10"
    api_endpoint_uri: "http://webservice-cs.tecdoc.net/pegasus-3-0/services/TecdocToCatDLB.jsonEndpoint"
    media_uri: "http://webservice-cs.tecdoc.net/pegasus-3-0/documents"
    category_slug_separator: '/' # Optional
    root_category_name_provider: service_id # Optional. Must implement Nfq\Bundle\TecDocBundle\Provider\RootCategoryNameProviderInterface
    used_tecdoc_filters_ids: []
```
