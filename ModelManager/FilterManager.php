<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ModelManager;

use Nfq\Bundle\TecDocBundle\ApiManager\TecDocApiManagerInterface;
use Nfq\Bundle\TecDocBundle\Exception\InvalidFilterValueTypeException;
use Nfq\Bundle\TecDocBundle\Entity\Filter;
use Nfq\Bundle\TecDocBundle\Entity\FilterValue;
use Nfq\Bundle\TecDocBundle\RequestMode;

class FilterManager
{
    const KEY_ARTICLE_LINKS = 'article_links';
    const KEY_FILTERS = 'filters';

    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @var array
     */
    private $usedTecDocFilters;

    /**
     * @param TecDocApiManagerInterface $apiManager
     * @param array $usedTecDocFilters
     */
    public function __construct(TecDocApiManagerInterface $apiManager, array $usedTecDocFilters)
    {
        $this->apiManager = $apiManager;
        $this->usedTecDocFilters = $usedTecDocFilters;
    }

    /**
     * @param int $genericArticleId
     * @param int $vehicleId
     * @param string $linkingTargetType
     * @param FilterValue[] $filterValues
     * @param array $articleIds
     * @return array
     */
    public function getFiltersWithArticleLinks(
        int $genericArticleId,
        int $vehicleId,
        string $linkingTargetType,
        array $filterValues = [],
        array $articleIds = []
    ): array {
        $attributeValues = [];

        if (!empty($filterValues)) {
            foreach ($filterValues as $value) {
                $value->setSelected(true);
                $attributeValues[] = $value->createTecDocItemStructure();
            }
        }

        $criteria = $this->apiManager->getCriteriaDialogAttributs(
            $genericArticleId,
            $vehicleId,
            $linkingTargetType,
            RequestMode::ARTICLES_AND_ATTRIBUTES,
            $attributeValues,
            $articleIds
        );

        if (empty($criteria)) {
            return [];
        }

        $criteria = \reset($criteria);

        $filters = [];

        if (!empty($criteria->attributes) && !empty($criteria->attributeValues)) {
            $filters = $this->createFilters($criteria->attributes, $criteria->attributeValues);
        }

        $articleLinks = !empty($criteria->articleLinkIds) ? $this->createArticleLinks($criteria->articleLinkIds) : [];

        return [
            self::KEY_ARTICLE_LINKS => $articleLinks,
            self::KEY_FILTERS => $filters,
        ];
    }

    /**
     * @param \stdClass $attributes
     * @param \stdClass $attributeValues
     * @return array
     */
    protected function createFilters(\stdClass $attributes, \stdClass $attributeValues): array
    {
        if (!isset($attributes->array) || !isset($attributeValues->array)) {
            return [];
        }

        $tecDocFilters = $attributes->array;
        $tecDocFilterValues = $attributeValues->array;
        $filters = [];

        $filterValues = [];
        foreach ($tecDocFilterValues as $tecDocFilterValue) {
            try {
                $filterValues[] = FilterValue::createFromTecDocItem($tecDocFilterValue);
            } catch (InvalidFilterValueTypeException $e) {
                continue;
            }
        }

        foreach ($tecDocFilters as $tecDocFilter) {
            $filter = Filter::createFromTecDocItem($tecDocFilter);

            if (!\in_array($filter->getId(), $this->usedTecDocFilters)) {
                continue;
            }

            foreach ($filterValues as $filterValue) {
                if ($filter->getId() == $filterValue->getId()) {
                    $filter->addValue($filterValue);
                }
            }

            if ($filter->hasValues()) {
                $filters[] = $filter;
            }
        }

        return $filters;
    }

    /**
     * @param \stdClass $articleLinkIds
     * @return array
     */
    protected function createArticleLinks(\stdClass $articleLinkIds): array
    {
        if (isset($articleLinkIds->array)) {
            return \array_map(
                static function (\stdClass $articleLink): array {
                    return \json_decode(
                        \json_encode($articleLink, \JSON_THROW_ON_ERROR, 512),
                        true,
                        512,
                        \JSON_THROW_ON_ERROR
                    );
                },
                $articleLinkIds->array
            );
        }

        return [];
    }
}
