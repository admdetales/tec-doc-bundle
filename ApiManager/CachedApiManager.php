<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\ApiManager;

use Nfq\Bundle\TecDocBundle\Cache\CacheKeyGenerator;
use Nfq\Bundle\TecDocBundle\Cache\CacheManager;
use Nfq\Bundle\TecDocBundle\ResultMode;
use Nfq\Bundle\TecDocBundle\SortMode;
use Nfq\Bundle\TecDocBundle\TargetType;

class CachedApiManager implements TecDocApiManagerInterface
{
    /**
     * @var TecDocApiManagerInterface
     */
    protected $apiManager;

    /**
     * @var CacheManager
     */
    protected $cacheManager;

    /**
     * @var CacheKeyGenerator
     */
    protected $keyGenerator;

    /**
     * @param TecDocApiManagerInterface $apiManager
     * @param CacheManager $cacheManager
     * @param CacheKeyGenerator $keyGenerator
     */
    public function __construct(
        TecDocApiManagerInterface $apiManager,
        CacheManager $cacheManager,
        CacheKeyGenerator $keyGenerator
    ) {
        $this->apiManager = $apiManager;
        $this->cacheManager = $cacheManager;
        $this->keyGenerator = $keyGenerator;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleDirectSearchAllNumbersWithState(
        string $articleNumber,
        int $numberType,
        bool $searchExact = true
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticleDirectSearchAllNumbersWithState(
            $articleNumber,
            $numberType,
            $searchExact
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleIdsWithState(
        array $genericArticleIds,
        int $linkingTargetId,
        string $linkingTargetType,
        array $brandNumbers = [],
        int $assemblyGroupNodeId = null
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticleIdsWithState(
            $genericArticleIds,
            $linkingTargetId,
            $linkingTargetType,
            $brandNumbers,
            $assemblyGroupNodeId
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getDirectArticlesByIds7(array $articleIds, string $language = null): array
    {
        $cacheKeyRequestMap = [];
        foreach ($articleIds as $articleId) {
            $cacheKey = $this->keyGenerator->generate([__FUNCTION__, $articleId, $language]);
            $cacheKeyRequestMap[$cacheKey] = $articleId;
        }

        $cachedResults = $this->cacheManager->cacheMultipleGet(\array_keys($cacheKeyRequestMap));
        foreach ($cachedResults as $cacheKey => $cachedResult) {
            if (null !== $cachedResult) {
                unset($cacheKeyRequestMap[$cacheKey]);
            }
        }

        if (empty($cacheKeyRequestMap)) {
            return $this->filterArray($cachedResults);
        }

        $apiResults = $this->apiManager->getDirectArticlesByIds7(\array_values($cacheKeyRequestMap), $language);
        $cacheKeyResponseMap = [];
        foreach ($apiResults as $apiResult) {
            if (isset($apiResult->directArticle, $apiResult->directArticle->articleId)) {
                $cacheKey = $this->keyGenerator->generate(
                    [
                        __FUNCTION__,
                        $apiResult->directArticle->articleId,
                        $language,
                    ]
                );
                $cacheKeyResponseMap[$cacheKey] = $apiResult;
            }
        }
        $this->cacheManager->cacheMultipleSet($this->removeNullValues($cacheKeyResponseMap));

        return $this->filterArray(\array_merge($cachedResults, $apiResults));
    }

    /**
     * {@inheritdoc}
     */
    public function getAssignedArticlesByIds7(
        int $linkingTargetId,
        string $linkingTargetType,
        int $manufacturerId,
        int $modelId,
        array $articlePairs
    ): array {
        $cacheKeyRequestMap = [];
        foreach ($articlePairs as $articlePair) {
            $cacheKey = $this->keyGenerator->generate(
                [__FUNCTION__, $linkingTargetId, $linkingTargetType, $manufacturerId, $modelId, $articlePair]
            );
            $cacheKeyRequestMap[$cacheKey] = $articlePair;
        }

        $cachedResults = $this->cacheManager->cacheMultipleGet(\array_keys($cacheKeyRequestMap));
        foreach ($cachedResults as $cacheKey => $cachedResult) {
            if (null !== $cachedResult) {
                unset($cacheKeyRequestMap[$cacheKey]);
            }
        }

        if (empty($cacheKeyRequestMap)) {
            return $this->filterArray($cachedResults);
        }

        $apiResults = $this->apiManager->getAssignedArticlesByIds7(
            $linkingTargetId,
            $linkingTargetType,
            $manufacturerId,
            $modelId,
            \array_values($cacheKeyRequestMap)
        );
        $cacheKeyResponseMap = [];
        foreach ($apiResults as $apiResult) {
            if (isset($apiResult->assignedArticle)
                && isset($apiResult->assignedArticle->articleId)
                && isset($apiResult->assignedArticle->articleLinkId)
            ) {
                $cacheKey = $this->keyGenerator->generate(
                    [
                        __FUNCTION__,
                        $linkingTargetId,
                        $linkingTargetType,
                        $manufacturerId,
                        $modelId,
                        [
                            'articleId' => $apiResult->assignedArticle->articleId,
                            'articleLinkId' => $apiResult->assignedArticle->articleLinkId,
                        ],
                    ]
                );
                $cacheKeyResponseMap[$cacheKey] = $apiResult;
            }
        }
        $this->cacheManager->cacheMultipleSet($this->removeNullValues($cacheKeyResponseMap));

        return $this->filterArray(\array_merge($cachedResults, $apiResults));
    }

    /**
     * {@inheritdoc}
     */
    public function getArticlePartList(int $articleId): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticlePartList($articleId);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticles(
        bool $searchTreeNodes = true,
        bool $linked = false,
        ?int $linkingTargetId = null,
        ?string $linkingTargetType = null
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getGenericArticles(
            $searchTreeNodes,
            $linked,
            $linkingTargetId,
            $linkingTargetType
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleAccessoryList4(int $articleId): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticleAccessoryList4($articleId);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetManufacturer2(int $articleId): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticleLinkedAllLinkingTargetManufacturer2($articleId);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTarget4(int $articleId, string $manufacturerId): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getArticleLinkedAllLinkingTarget4($articleId, $manufacturerId);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getArticleLinkedAllLinkingTargetsByIds3(int $articleId, array $linkedArticlePairs): array
    {
        $cacheKeyRequestMap = [];
        foreach ($linkedArticlePairs as $key => $linkedArticlePair) {
            $cacheKey = $this->keyGenerator->generate([__FUNCTION__, $articleId, $linkedArticlePair]);
            $cacheKeyRequestMap[$cacheKey] = $linkedArticlePair;
        }

        $cachedResults = $this->cacheManager->cacheMultipleGet(\array_keys($cacheKeyRequestMap));
        foreach ($cachedResults as $cacheKey => $cachedResult) {
            if (null !== $cachedResult) {
                unset($cacheKeyRequestMap[$cacheKey]);
            }
        }

        if (empty($cacheKeyRequestMap)) {
            return $this->filterArray($cachedResults);
        }

        $apiResults = $this->apiManager->getArticleLinkedAllLinkingTargetsByIds3(
            $articleId,
            \array_values($cacheKeyRequestMap)
        );
        $cacheKeyResponseMap = [];
        foreach ($apiResults as $apiResult) {
            if (isset($apiResult->articleLinkId, $apiResult->linkingTargetId)) {
                $cacheKey = $this->keyGenerator->generate(
                    [
                        __FUNCTION__,
                        $articleId,
                        [
                            'articleLinkId' => $apiResult->articleLinkId,
                            'linkingTargetId' => $apiResult->linkingTargetId,
                        ],
                    ]
                );
                $cacheKeyResponseMap[$cacheKey] = $apiResult;
            }
        }
        $this->cacheManager->cacheMultipleSet($this->removeNullValues($cacheKeyResponseMap));

        return $this->filterArray(\array_merge($cachedResults, $apiResults));
    }

    /**
     * {@inheritdoc}
     */
    public function getManufacturers(string $linkingTargetType = TargetType::PASSENGER_CAR): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getManufacturers($linkingTargetType);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getModelSeries(int $manufacturerId, string $linkingTargetType): array
    {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getModelSeries($manufacturerId, $linkingTargetType);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleIdsByCriteria(
        int $manufacturerId,
        int $modelId,
        string $carType = TargetType::PASSENGER_CAR
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getVehicleIdsByCriteria($manufacturerId, $modelId, $carType);

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getVehicleByIds4(array $vehicleIds): array
    {
        $cacheKeyRequestMap = [];
        foreach ($vehicleIds as $key => $vehicleId) {
            $cacheKey = $this->keyGenerator->generate([__FUNCTION__, $vehicleId]);
            $cacheKeyRequestMap[$cacheKey] = $vehicleId;
        }

        $cachedResults = $this->cacheManager->cacheMultipleGet(\array_keys($cacheKeyRequestMap));
        foreach ($cachedResults as $cacheKey => $cachedResult) {
            if (null !== $cachedResult) {
                unset($cacheKeyRequestMap[$cacheKey]);
            }
        }

        if (empty($cacheKeyRequestMap)) {
            return $this->filterArray($cachedResults);
        }

        $apiResults = $this->apiManager->getVehicleByIds4(\array_values($cacheKeyRequestMap));
        $cacheKeyResponseMap = [];
        foreach ($apiResults as $apiResult) {
            if (isset($apiResult->carId)) {
                $cacheKey = $this->keyGenerator->generate([__FUNCTION__, $apiResult->carId]);
                $cacheKeyResponseMap[$cacheKey] = $apiResult;
            }
        }
        $this->cacheManager->cacheMultipleSet($this->removeNullValues($cacheKeyResponseMap));

        return $this->filterArray(\array_merge($cachedResults, $apiResults));
    }

    /**
     * {@inheritdoc}
     */
    public function getCriteriaDialogAttributs(
        int $genericArticleId,
        int $linkingTargetId,
        string $linkingTargetType,
        string $mode,
        array $attributeValues = [],
        array $articleIds = []
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getCriteriaDialogAttributs(
            $genericArticleId,
            $linkingTargetId,
            $linkingTargetType,
            $mode,
            $attributeValues,
            $articleIds
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildNodesAllLinkingTarget2(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $parentId = null,
        bool $includeChildNodes = true
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getChildNodesAllLinkingTarget2(
            $linkingTargetType,
            $linkingTargetId,
            $parentId,
            $includeChildNodes
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function getGenericArticlesByManufacturer6(
        string $linkingTargetType,
        string $linkingTargetId = null,
        int $assemblyGroupNodeId = null,
        array $brandNumbers = [],
        array $genericArticleId = [],
        int $resultMode = ResultMode::DISTINCT_GENERIC_ARTICLES,
        int $sortMode = SortMode::BRAND_NAME
    ): array {
        $cacheKey = $this->keyGenerator->generate([__FUNCTION__, \func_get_args()]);
        if (null !== $result = $this->cacheManager->cacheGet($cacheKey)) {
            return $result;
        }

        $result = $this->apiManager->getGenericArticlesByManufacturer6(
            $linkingTargetType,
            $linkingTargetId,
            $assemblyGroupNodeId,
            $brandNumbers,
            $genericArticleId,
            $resultMode,
            $sortMode
        );

        $this->cacheManager->cacheSet($cacheKey, $result);

        return $result;
    }

    /**
     * @param array $array
     * @return array
     */
    private function filterArray(array $array): array
    {
        $array = \array_values($array);
        $array = $this->removeNullValues($array);

        return $array;
    }

    /**
     * @param array $array
     * @return array
     */
    private function removeNullValues(array $array): array
    {
        return \array_filter(
            $array,
            static function ($element): bool {
                return null !== $element;
            }
        );
    }
}
