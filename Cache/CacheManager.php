<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Cache;

use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;

class CacheManager
{
    /**
     * @var CacheInterface
     */
    protected $cache;

    /**
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function cacheGet(string $key)
    {
        try {
            return $this->unserializeValue($this->cache->get($key, null));
        } catch (\Exception | InvalidArgumentException $e) {
            return null;
        }
    }

    /**
     * @param array $keys
     * @return array
     */
    public function cacheMultipleGet(array $keys): array
    {
        try {
            $cachedResults = [];
            foreach ($this->cache->getMultiple($keys, null) as $key => $cachedResult) {
                $cachedResults[$key] = $this->unserializeValue($cachedResult);
            }

            return $cachedResults;
        } catch (\Exception | InvalidArgumentException $e) {
            return (array)\array_combine($keys, \array_fill(0, \count($keys), null));
        }
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function cacheSet(string $key, $value): bool
    {
        try {
            return $this->cache->set($key, $this->serializeValue($value));
        } catch (\Exception | InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * @param array $values
     * @return bool
     */
    public function cacheMultipleSet(array $values): bool
    {
        try {
            foreach ($values as $key => $value) {
                $values[$key] = $this->serializeValue($value);
            }

            return $this->cache->setMultiple($values);
        } catch (\Exception | InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * @param mixed $value
     * @return string
     */
    protected function serializeValue($value): string
    {
        return \serialize($value);
    }

    /**
     * @param string|null $value
     * @return mixed|null
     */
    protected function unserializeValue(string $value = null)
    {
        return null !== $value ? \unserialize($value) : null;
    }
}
