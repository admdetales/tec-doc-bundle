<?php

/**
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace Nfq\Bundle\TecDocBundle\Resolver;

use Nfq\Bundle\TecDocBundle\Entity\Vehicle;
use Nfq\Bundle\TecDocBundle\TargetType;

class VehicleTargetTypeResolver
{
    /**
     * Minimal ID of commercial vehicle
     */
    private const MIN_COMMERCIAL_VEHICLE_ID = 1000000;

    /**
     * @param Vehicle $vehicle
     * @return string
     */
    public function resolve(Vehicle $vehicle): string
    {
        if ($vehicle->getId() >= self::MIN_COMMERCIAL_VEHICLE_ID) {
            return TargetType::COMMERCIAL_CAR;
        }

        return TargetType::PASSENGER_CAR;
    }
}
